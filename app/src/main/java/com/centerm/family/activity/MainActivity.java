package com.centerm.family.activity;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.centerm.family.R;
import com.centerm.family.base.BaseActivity;
import com.centerm.family.dialog.SpinerPopWindow;
import com.centerm.family.view.RoundProgressBar;
import com.centerm.family.view.TimeView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {
    private RoundProgressBar mRoundProgressBar;
    private LinearLayout mLlTimer;
    private TimeView timeView;
    private TextView mTvCurrentName;
    private List<String> nameList = new ArrayList<>();
    private SpinerPopWindow mSpinerPopWindow;

    @Override
    protected void initDataAndEvent() {
        initUsualToolbar("运动");
        mBtnBack.setVisibility(View.GONE);
        setMenuText("统计");

        mRoundProgressBar = bindView(R.id.mRoundProgressBar);
        mRoundProgressBar.setProgress(100);

        mLlTimer = bindView(R.id.mLlTimer);
        timeView = new TimeView(this, current -> {

        });
        mLlTimer.addView(timeView);

        nameList.add("爷爷");
        nameList.add("奶奶");
        nameList.add("外公");
        nameList.add("外婆");
        mTvCurrentName = bindView(R.id.mTvCurrentName);
        mTvCurrentName.setText(nameList.get(0));
        mTvCurrentName.setOnClickListener(view -> {
            mSpinerPopWindow.setWidth(mTvCurrentName.getWidth());
            mSpinerPopWindow.showAsDropDown(mTvCurrentName);
        });
        mSpinerPopWindow = new SpinerPopWindow(this);
        mSpinerPopWindow.refreshData(nameList, 0);
        mSpinerPopWindow.setItemListener(pos -> {
            mTvCurrentName.setText(nameList.get(pos));
        });

        findViewById(R.id.mBtnTrace).setOnClickListener(view -> startAct(TraceActivity.class));
        findViewById(R.id.mBtnSleep).setOnClickListener(view -> startAct(SleepActivity.class));
        findViewById(R.id.mBtnFence).setOnClickListener(view -> startAct(FenceActivity.class));
        findViewById(R.id.mBtnSetting).setOnClickListener(view -> startAct(SettingsActivity.class));



    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }


}
