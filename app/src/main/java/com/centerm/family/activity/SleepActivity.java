package com.centerm.family.activity;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.centerm.family.R;
import com.centerm.family.base.BaseActivity;
import com.centerm.family.dialog.SpinerPopWindow;
import com.centerm.family.view.RoundProgressBar;
import com.centerm.family.view.TimeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liubit on 2017/3/22.
 * 睡眠
 */

public class SleepActivity extends BaseActivity {
    private RoundProgressBar mRoundProgressBar;
    private LinearLayout mLlTimer;
    private TextView mTvCurrentName;
    private TimeView timeView;
    private List<String> nameList = new ArrayList<>();
    private SpinerPopWindow mSpinerPopWindow;

    @Override
    protected void initDataAndEvent() {
        initUsualToolbar("睡眠");
        setMenuText("统计");

        mRoundProgressBar = bindView(R.id.mRoundProgressBar);
        mRoundProgressBar.setProgress(100);
        mTvCurrentName = bindView(R.id.mTvCurrentName);
        mTvCurrentName.setOnClickListener(view -> {
            mSpinerPopWindow.setWidth(mTvCurrentName.getWidth());
            mSpinerPopWindow.showAsDropDown(mTvCurrentName);
        });

        mLlTimer = bindView(R.id.mLlTimer);
        timeView = new TimeView(this, new TimeView.MyTimeListener() {
            @Override
            public void time(long current) {

            }
        });
        mLlTimer.addView(timeView);

        nameList.add("爷爷");
        nameList.add("奶奶");
        nameList.add("外公");
        nameList.add("外婆");
        mTvCurrentName.setText(nameList.get(0));
        mSpinerPopWindow = new SpinerPopWindow(this);
        mSpinerPopWindow.refreshData(nameList, 0);
        mSpinerPopWindow.setItemListener(pos -> {
            mTvCurrentName.setText(nameList.get(pos));
        });


    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_sleep;
    }





}
