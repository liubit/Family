package com.centerm.family.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.centerm.family.R;
import com.centerm.family.adapter.RelateAdapter;
import com.centerm.family.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liubit on 2017/3/22.
 */

public class RelateActivity extends BaseActivity {
    private ListView mListView;
    private RelateAdapter adapter;
    private List<String> data;

    @Override
    protected void initDataAndEvent() {
        initUsualToolbar("已关联的老人");

        mListView = bindView(R.id.mListView);
        data = new ArrayList<>();
        data.add("爷爷");
        data.add("奶奶");
        data.add("外公");
        data.add("外婆");
        adapter = new RelateAdapter(this, data);
        adapter.setRelateItemClick(p -> {
            Intent intent = new Intent(RelateActivity.this, RelateDetailActivity.class);
            intent.putExtra("name", data.get(p));
            startActivity(intent);
        });
        mListView.setAdapter(adapter);



    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_relate;
    }

}
