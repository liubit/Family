package com.centerm.family.activity;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.model.LatLng;
import com.centerm.family.R;
import com.centerm.family.base.BaseActivity;
import com.centerm.family.dialog.SpinerPopWindow;
import com.centerm.family.view.TimeView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by liubit on 2017/3/22.
 * 运动轨迹
 */

public class TraceActivity extends BaseActivity {
    private MapView mMapView;
    private BaiduMap mBaiduMap;
    private LinearLayout mLlTrace;
    private TimeView timeView;
    private boolean isFirstLoc = true; // 是否首次定位
    public MyLocationListenner myListener = new MyLocationListenner();
    private LocationClient mLocClient;
    private List<LatLng> points;
    private LatLng ll;

    private TextView mTvCurrentName;
    private List<String> nameList = new ArrayList<>();
    private SpinerPopWindow mSpinerPopWindow;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        SDKInitializer.initialize(getApplicationContext());
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initDataAndEvent() {
        initUsualToolbar("运动轨迹");
        setMenuText("统计");

        mTvCurrentName = bindView(R.id.mTvCurrentName);
        mTvCurrentName.setOnClickListener(view -> {
            mSpinerPopWindow.setWidth(mTvCurrentName.getWidth());
            mSpinerPopWindow.showAsDropDown(mTvCurrentName);
        });

        mLlTrace = bindView(R.id.mLlTrace);
        timeView = new TimeView(this, new TimeView.MyTimeListener() {
            @Override
            public void time(long current) {

            }
        });
        mLlTrace.addView(timeView);

        nameList.add("爷爷");
        nameList.add("奶奶");
        nameList.add("外公");
        nameList.add("外婆");
        mTvCurrentName.setText(nameList.get(0));
        mSpinerPopWindow = new SpinerPopWindow(this);
        mSpinerPopWindow.refreshData(nameList, 0);
        mSpinerPopWindow.setItemListener(pos -> {
            mTvCurrentName.setText(nameList.get(pos));
        });

        initMap();
        makeTrace();

    }

    private void initMap(){
        mMapView = bindView(R.id.mapView);
        mBaiduMap = mMapView.getMap();
        mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL); //设置为普通模式的地图
        // 开启定位图层
        mBaiduMap.setMyLocationEnabled(true);
        mLocClient = new LocationClient(this);  //定位用到的一个类
        mLocClient.registerLocationListener(myListener); //注册监听

        // LocationClientOption类用来设置定位SDK的定位方式，
        LocationClientOption option = new LocationClientOption(); //以下是给定位设置参数
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);//设置成定位模式
        option.setOpenGps(true); // 打开gps
        option.setCoorType("bd09ll"); // 设置坐标类型
        option.setScanSpan(3000);
        mLocClient.setLocOption(option);

        mLocClient.start();

    }

    /**绘制两个坐标点之间的线段,从以前位置到现在位置*/
    private void makeTrace() {
        if(points==null){
            points = new ArrayList<>();
        }
        LatLng llStart = new LatLng(40.006821, 116.493271);
        LatLng llEnd = new LatLng(40.006821, 116.494271);
        points.add(llStart);
        points.add(new LatLng(40.007821, 116.493271));
        points.add(new LatLng(40.007521, 116.494271));
        points.add(llEnd);
        //points.add(new LatLng(40.006821, 116.493271));
        // 绘制一个大地曲线
        OverlayOptions ooPolyline = new PolylineOptions().width(15).color(0xAAFF0000).points(points);
        mBaiduMap.addOverlay(ooPolyline);

        MarkerOptions markerOption = new MarkerOptions();
        markerOption.position(llStart);
        markerOption.draggable(false);
        markerOption.icon(
                BitmapDescriptorFactory.fromBitmap(BitmapFactory
                        .decodeResource(getResources(), R.mipmap.icon_start)));
        mBaiduMap.addOverlay(markerOption);

        MarkerOptions markerOption2 = new MarkerOptions();
        markerOption2.position(llEnd);
        markerOption2.draggable(false);
        markerOption2.icon(
                BitmapDescriptorFactory.fromBitmap(BitmapFactory
                        .decodeResource(getResources(), R.mipmap.icon_end)));
        mBaiduMap.addOverlay(markerOption2);


    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_trace;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /// 关闭定位图层
        mBaiduMap.setMyLocationEnabled(false);
        mMapView.onDestroy();
        mLocClient.stop();
    }
    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    public class MyLocationListenner implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location) {
            // map view 销毁后不在处理新接收的位置
            if (location == null || mMapView == null) {
                return;
            }
            MyLocationData locData = new MyLocationData.Builder()
                    .accuracy(location.getRadius())
                    // 此处设置开发者获取到的方向信息，顺时针0-360
                    .direction(100).latitude(location.getLatitude())
                    .longitude(location.getLongitude()).build();
            mBaiduMap.setMyLocationData(locData);
            ll = new LatLng(location.getLatitude(), location.getLongitude());
            if (isFirstLoc) {
                isFirstLoc = false;
                Log.d("===", "onReceiveLocation:"+location.getLatitude()+"----"+location.getLongitude());
                MapStatus.Builder builder = new MapStatus.Builder();
                builder.target(ll).zoom(18.0f);
                mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));
            }

//            OverlayOptions fenceOverlay = new CircleOptions().fillColor(0x000000FF).center(ll)
//                    .stroke(new Stroke(5, Color.rgb(0xff, 0x00, 0x33)))
//                    .radius(800);//画出圆形界限
//            mBaiduMap.addOverlay(fenceOverlay);
        }

        @Override
        public void onConnectHotSpotMessage(String s, int i) {

        }
    }
}
