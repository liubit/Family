package com.centerm.family.activity;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.CircleOptions;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.Stroke;
import com.baidu.mapapi.model.LatLng;
import com.centerm.family.R;
import com.centerm.family.base.BaseActivity;
import com.centerm.family.dialog.SpinerPopWindow;
import com.centerm.family.tools.UITool;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liubit on 2017/3/22.
 * 电子围栏
 */

public class FenceActivity extends BaseActivity {
    private MapView mMapView;
    private BaiduMap mBaiduMap;
    private LatLng currentLatLng;
    private MarkerOptions currentMarkerOptions;

    private TextView mTvCurrentName;
    private List<String> nameList = new ArrayList<>();
    private SpinerPopWindow mSpinerPopWindow;

    @Override
    protected void initDataAndEvent() {
        initUsualToolbar("电子围栏");

        nameList.add("爷爷");
        nameList.add("奶奶");
        nameList.add("外公");
        nameList.add("外婆");
        mTvCurrentName = bindView(R.id.mTvCurrentName);
        mTvCurrentName.setText(nameList.get(0));
        mSpinerPopWindow = new SpinerPopWindow(this);
        mSpinerPopWindow.refreshData(nameList, 0);
        mSpinerPopWindow.setItemListener(pos -> mTvCurrentName.setText(nameList.get(pos)));
        mTvCurrentName.setOnClickListener(view -> {
            mSpinerPopWindow.setWidth(mTvCurrentName.getWidth());
            int offY = UITool.dip2px(36*nameList.size()+1);
            mSpinerPopWindow.showUp(mTvCurrentName, 0, offY);
        });

        initMap();

    }

    private void initMap(){
        mMapView = bindView(R.id.mapView);
        mBaiduMap = mMapView.getMap();

        LatLng ll = new LatLng(40.006821, 116.493271);
        MapStatus.Builder builder = new MapStatus.Builder();
        builder.target(ll).zoom(15.0f);
        mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));

        MarkerOptions markerOption = new MarkerOptions();
        markerOption.position(ll);
        markerOption.draggable(false);
        markerOption.icon(
                BitmapDescriptorFactory.fromBitmap(BitmapFactory
                        .decodeResource(getResources(), R.mipmap.icon_map_big)));
        mBaiduMap.addOverlay(markerOption);
        getFence(ll);

        currentLatLng = new LatLng(40.016821, 116.494271);
        currentMarkerOptions = new MarkerOptions();
        currentMarkerOptions.position(currentLatLng);
        currentMarkerOptions.draggable(false);
        currentMarkerOptions.icon(
                BitmapDescriptorFactory.fromBitmap(BitmapFactory
                        .decodeResource(getResources(), R.mipmap.icon_gcoding)));
        mBaiduMap.addOverlay(currentMarkerOptions);


        mBaiduMap.setOnMapClickListener(new BaiduMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                //获取经纬度
                double latitude = latLng.latitude;
                double longitude = latLng.longitude;
                System.out.println("latitude=" + latitude + ",longitude=" + longitude);
                //先清除图层
                mBaiduMap.clear();

                // 构建MarkerOption，用于在地图上添加Marker
                MarkerOptions options = new MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                .decodeResource(getResources(), R.mipmap.icon_map_big)));
                // 在地图上添加Marker，并显示
                mBaiduMap.addOverlay(options);
                mBaiduMap.addOverlay(currentMarkerOptions);
                getFence(latLng);

            }

            @Override
            public boolean onMapPoiClick(MapPoi mapPoi) {
                return false;
            }
        });
    }

    private void getFence(LatLng latLng){
        OverlayOptions fenceOverlay = new CircleOptions().fillColor(0x000000FF).center(latLng)
                    .stroke(new Stroke(5, Color.rgb(0xff, 0x00, 0x33)))
                    .radius(800);//画出圆形界限
        mBaiduMap.addOverlay(fenceOverlay);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_fence;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        SDKInitializer.initialize(getApplicationContext());
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBaiduMap.setMyLocationEnabled(false);
        mMapView.onDestroy();
    }
    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();

    }
    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

}
