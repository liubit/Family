package com.centerm.family.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import com.centerm.family.R;
import com.centerm.family.base.BaseActivity;
import com.centerm.family.tools.UITool;
import com.xys.libzxing.zxing.encoding.EncodingUtils;

/**
 * Created by liubit on 2017/3/22.
 */

public class QrCodeActivity extends BaseActivity {
    private ImageView mIvQrCode;

    @Override
    protected void initDataAndEvent() {
        initUsualToolbar("我的二维码");

        mIvQrCode = bindView(R.id.mIvQrCode);
        Bitmap logo = BitmapFactory.decodeResource(getResources(), R.mipmap.app_icon);
        Bitmap bitmap = EncodingUtils.createQRCode("123456", UITool.dip2px(300), UITool.dip2px(300), logo);
        mIvQrCode.setImageBitmap(bitmap);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_qrcode;
    }
}
