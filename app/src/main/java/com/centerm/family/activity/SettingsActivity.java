package com.centerm.family.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.centerm.family.R;
import com.centerm.family.base.BaseActivity;
import com.xys.libzxing.zxing.activity.CaptureActivity;

/**
 * Created by liubit on 2017/3/22.
 * 设置
 */

public class SettingsActivity extends BaseActivity {

    @Override
    protected void initDataAndEvent() {
        initUsualToolbar("设置");

        findViewById(R.id.mBtnRelate).setOnClickListener(view -> startAct(RelateActivity.class));
        findViewById(R.id.mBtnQrCode).setOnClickListener(view -> startAct(QrCodeActivity.class));
        findViewById(R.id.mBtnScan).setOnClickListener(view -> {
            startActivityForResult(new Intent(getBaseContext(), CaptureActivity.class),0);
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode== Activity.RESULT_OK){
            Bundle bundle = data.getExtras();
            Toast.makeText(getApplicationContext(), bundle.getString("result"), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_settings;
    }

}
