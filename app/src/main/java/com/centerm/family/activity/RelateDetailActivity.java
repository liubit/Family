package com.centerm.family.activity;

import android.view.View;
import android.widget.TextView;

import com.centerm.family.R;
import com.centerm.family.base.BaseActivity;

/**
 * Created by liubit on 2017/3/22.
 */

public class RelateDetailActivity extends BaseActivity {

    @Override
    protected void initDataAndEvent() {
        initUsualToolbar("已关联的老人");

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_relate_detail;
    }
}
