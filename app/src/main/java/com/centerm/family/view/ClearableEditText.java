package com.centerm.family.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.EditText;

import com.centerm.family.R;

/**
 * Created by liubit on 2015/12/12.
 */
public class ClearableEditText extends EditText {
    private Drawable dRight;
    private Rect rBounds;

//    public ClearableEditText(Context paramContext) {
//        super(paramContext);
//        initEditText(paramContext);
//    }

    public ClearableEditText(Context paramContext, AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
        initEditText(paramContext,paramAttributeSet);
    }

    public ClearableEditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
        super(paramContext, paramAttributeSet, paramInt);
        initEditText(paramContext, paramAttributeSet);
    }

    // 初始化edittext 控件
    private void initEditText(Context context,AttributeSet attr) {
        Drawable drawable;
        TypedArray array = context.obtainStyledAttributes(attr, R.styleable.ClearableEditText);
        Drawable icon = array.getDrawable(R.styleable.ClearableEditText_icon_);
        if(icon!=null){
            drawable = icon;
        }else {
            drawable = getResources().getDrawable(R.mipmap.clear_edittext);
        }
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight()); //设置边界
        setCompoundDrawables(null, null, drawable, null);

        setEditTextDrawable();

        addTextChangedListener(new TextWatcher() { // 对文本内容改变进行监听
            @Override
            public void afterTextChanged(Editable paramEditable) {
            }

            @Override
            public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
            }

            @Override
            public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
                ClearableEditText.this.setEditTextDrawable();
            }
        });
    }

    // 控制图片的显示
    public void setEditTextDrawable() {
        if (getText().toString().length() == 0) {
            setCompoundDrawables(null, null, null, null);
        } else {
            setCompoundDrawables(null, null, this.dRight, null);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.dRight = null;
        this.rBounds = null;

    }

    /**
     * 添加触摸事件 点击之后 出现 清空editText的效果
     */
    @Override
    public boolean onTouchEvent(MotionEvent paramMotionEvent) {
        if ((this.dRight != null) && (paramMotionEvent.getAction() == 1)) {
            this.rBounds = this.dRight.getBounds();

            boolean touchable = paramMotionEvent.getX() > (getWidth() - getTotalPaddingRight())
                    && (paramMotionEvent.getX() < ((getWidth() - getPaddingRight())));
            if(touchable){
                setText("");
                paramMotionEvent.setAction(MotionEvent.ACTION_CANCEL);
            }

        }
        return super.onTouchEvent(paramMotionEvent);
    }

    /**
     * 显示右侧X图片的
     * 左上右下
     */
    @Override
    public void setCompoundDrawables(Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, Drawable paramDrawable4) {
        if (paramDrawable3 != null)
            this.dRight = paramDrawable3;
        super.setCompoundDrawables(paramDrawable1, paramDrawable2, paramDrawable3, paramDrawable4);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if(focused){
            setEditTextDrawable();
        }else{
            setCompoundDrawables(null, null, null, null);
        }

    }

}
