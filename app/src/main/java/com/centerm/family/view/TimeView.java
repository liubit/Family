package com.centerm.family.view;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.centerm.family.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by king on 2017/3/22.
 */

public class TimeView extends LinearLayout {

    public TextView left;
    public TextView right;
    public TextView time;
    public LayoutInflater mInflater;

    public long currenttime;

    public String mYear;
    public String mMonth;
    public String mDay;
    public Context context;

    public MyTimeListener myTimeListener;

    public TimeView(Context context, MyTimeListener myTimeListener) {
        super(context);
        this.context = context;
        this.myTimeListener = myTimeListener;
        initView(context);
    }

    public void initView(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInflater.inflate(R.layout.view_time, this);
        left = (TextView) findViewById(R.id.left);
        time = (TextView) findViewById(R.id.time);
        right = (TextView) findViewById(R.id.right);

        currenttime = System.currentTimeMillis();
        changeDate();

        left.setOnClickListener(leftListener);
        time.setOnClickListener(timeListener);
        right.setOnClickListener(rightListener);
    }

    OnClickListener timeListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            new DatePickerDialog(context , new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    mYear = year+"";
                    mMonth = (month+1)+"";
                    if (mMonth.length()==1) mMonth = "0"+mMonth;
                    mDay = day+"";
                    if (mDay.length()==1) mDay = "0"+mDay;
                    changeTime();
                    myTimeListener.time(currenttime);
                }
            }, Integer.parseInt(mYear), Integer.parseInt(mMonth)-1,Integer.parseInt(mDay)).show();
        }
    };

    OnClickListener leftListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            currenttime = currenttime-(1000 * 60 * 60 *24);
            changeDate();
            myTimeListener.time(currenttime);
        }
    };

    OnClickListener rightListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            currenttime = currenttime+(1000 * 60 * 60 *24);
            changeDate();
            myTimeListener.time(currenttime);
        }
    };

    public void changeDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日");
        Date curDate = new Date(currenttime);//获取当前时间
        String str = formatter.format(curDate);
        mYear = str.substring(0,4);
        mMonth = str.substring(str.indexOf("年")+1,str.indexOf("月"));
        mDay = str.substring(str.indexOf("月")+1,str.indexOf("日"));
        time.setText(str);
    }

    public void changeTime(){
        time.setText(mYear+"年"+mMonth+"月"+mDay+"日");
        String times = mYear+"-"+mMonth+"-"+mDay+"-"+"01-"+"01-"+"01";
        SimpleDateFormat sdr = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss",
                Locale.CHINA);
        Date date;
        try {
            date = sdr.parse(times);
            long l = date.getTime();
            currenttime = l;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface MyTimeListener {
        public void time(long current);
    }
}
