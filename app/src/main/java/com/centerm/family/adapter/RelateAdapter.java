package com.centerm.family.adapter;

/**
 * Created by liubit on 2017/3/20.
 */

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.centerm.family.R;
import com.centerm.family.view.SwipeLayout;

import java.util.ArrayList;
import java.util.List;

public class RelateAdapter extends BaseAdapter {
    private Context context;
    private List<String> list;
    private ArrayList<SwipeLayout> openedItems;
    private RelateItemClick relateItemClick;

    public RelateAdapter(Context context, List<String> list) {
        super();
        this.context=context;
        this.list=list;
        openedItems = new ArrayList<>();

    }

    public void setRelateItemClick(RelateItemClick itemClick){
        relateItemClick = itemClick;
    }

    @Override

    public int getCount() {
        return list==null?0:list.size();
    }

    @Override

    public Object getItem(int position) {
        return list == null ? 0 : position;
    }

    @Override

    public long getItemId(int position) {
        return 0;
    }

    @Override

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null){
            holder = new ViewHolder();
            convertView = View.inflate(context, R.layout.adapter_relate, null);
            holder.mBtnName = (TextView) convertView.findViewById(R.id.mBtnName);
            holder.mBtnDistance = (TextView) convertView.findViewById(R.id.mBtnDistance);
            holder.mBtnDelete = (LinearLayout) convertView.findViewById(R.id.mBtnDelete);
            holder.mRelateItem = (RelativeLayout) convertView.findViewById(R.id.mRelateItem);
            convertView.setTag(holder);
        }else{
            holder=(ViewHolder) convertView.getTag();
        }
        holder.mRelateItem.setOnClickListener(view -> {
            if(relateItemClick!=null){
                relateItemClick.itemClick(position);
            }
        });
        holder.mBtnName.setText(list.get(position));
        holder.mBtnDelete.setOnClickListener(view -> {
            list.remove(position);
            notifyDataSetChanged();
        });


        SwipeLayout sl = (SwipeLayout)convertView;
        sl.setOnSwipeListener(new SwipeLayout.OnSwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                openedItems.remove(layout);
            }

            @Override
            public void onOpen(SwipeLayout layout) {
                openedItems.add(layout);
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {
                // 关闭所有已经打开的条目
                for (int i = 0; i < openedItems.size(); i++) {
                    openedItems.get(i).close(true);
                }
                openedItems.clear();}

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

        });

        return convertView;

    }

    class ViewHolder{
        public TextView mBtnName;
        public TextView mBtnDistance;
        public LinearLayout mBtnDelete;
        public RelativeLayout mRelateItem;
    }

    public interface RelateItemClick{
        void itemClick(int p);
    }

}