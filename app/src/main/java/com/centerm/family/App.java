package com.centerm.family;

import android.app.Application;
import android.content.Context;

/**
 * Created by liubit on 2017/3/23.
 */

public class App extends Application {
    private static App instance;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        context = getApplicationContext();
    }

    public static App getApp(){
        return instance;
    }

    public static Context getContext() {
        return context;
    }

}
