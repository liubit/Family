package com.centerm.family.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.centerm.family.R;


/**
 * Created by liubit on 2017/2/23.
 */

public abstract class BaseActivity extends AppCompatActivity {
    public String TAG = this.getClass().getSimpleName();
    public LinearLayout mBtnBack;
    public LinearLayout mLlMenu;
    public TextView menu;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        doSomethingBeforeSetContentView();
        setContentView(LayoutInflater.from(this).inflate(getLayoutId(), null));
        initView();
        initDataAndEvent();
    }

    public void initUsualToolbar(int title){
        ((TextView)findViewById(R.id.mTvTitle)).setText(title);
        mBtnBack = bindView(R.id.mLlBack);
        mBtnBack.setOnClickListener(view -> finish());
        menu = bindView(R.id.mBtnMenu);
        mLlMenu = bindView(R.id.mLlMenu);
        setSupportActionBar((Toolbar)findViewById(R.id.mToolbar));
    }

    public void initUsualToolbar(String title){
        ((TextView)findViewById(R.id.mTvTitle)).setText(title);
        mBtnBack = bindView(R.id.mLlBack);
        mBtnBack.setOnClickListener(view -> finish());
        menu = bindView(R.id.mBtnMenu);
        mLlMenu = bindView(R.id.mLlMenu);
        setSupportActionBar((Toolbar)findViewById(R.id.mToolbar));
    }

    public  <T extends View> T bindView(@IdRes int id){
        View viewById = findViewById(id);
        return (T) viewById;
    }

    public void setMenuText(String t){
        if(TextUtils.isEmpty(t)){
            mLlMenu.setVisibility(View.GONE);
        }else {
            mLlMenu.setVisibility(View.VISIBLE);
            menu.setText(t);
        }



    }

    public void replaceFragment(int containerId, Fragment fragment) {
        FragmentTransaction trans = getSupportFragmentManager()
                .beginTransaction();
        trans.replace(containerId, fragment);
        trans.commitAllowingStateLoss();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void startAct(Class cla){
        startActivity(new Intent(BaseActivity.this, cla));
    }

    protected void initView(){}
    protected abstract void initDataAndEvent();
    protected abstract int getLayoutId();
    protected void doSomethingBeforeSetContentView(){}

}
